<html>
	<head>
		<title>Ejemplo Resolver Operaciones</title>
	</head>
	<body>
		<h1> Resuelva las expresiones </h1>
	
		<?php
			$i = 9;
			$f = 33.5;
			$c = 'X';
			
			/* && si tanto a como b son true
               || si cualquiera de a o b son true */
			   
			echo ($i >= 6) && ($c == 'X'), "<br>";         //= true
			echo ($i >= 6) || ($c == 12), "<br>";          //= true
			echo ($f < 11) && ($i > 100), "<br>";          //= true
			echo ($c != 'P') || (($i + $f) <= 10), "<br>"; //= true
			echo  $i + $f <= 10 ,"<br>";                   //= true
			echo  $i >= 6 && $c == 'X', "<br>";            //= true
			echo  $c != 'P' || $i + $f <= 10,  "<br>";     //= true	
		?>
	</body>
</html>